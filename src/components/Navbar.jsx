import "./navbar.scss";
import { useState } from "react";
import { Search, Notifications, ArrowDropDown } from "@material-ui/icons";

const Navbar = () => {
  const [isScrolled, setIsScrolled] = useState(false);

  window.onscroll = () => {
    setIsScrolled(window.pageYOffset === 0 ? false : true);
    return () => {
      window.pageYOffset = null;
    };
  };

  return (
    <div className={isScrolled ? "navbar scolled" : "navbar"}>
      <div className="container">
        <div className="left">
          <img
            alt=""
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Netflix_2015_logo.svg/799px-Netflix_2015_logo.svg.png"
          />
          <span>Home</span>
          <span>Series</span>
          <span>Movies</span>
          <span>New and popular</span>
          <span>My List</span>
        </div>
        <div className="right">
          <Search className="icon" />
          <span>KID</span>
          <Notifications className="icon" />
          <img
            src="https://images.pexels.com/photos/9940879/pexels-photo-9940879.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
            alt="profile pic"
          />
          <div className="profile">
            <ArrowDropDown className="icon" />
            <div className="options">
              <span>Settings</span>
              <span>Logout</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
