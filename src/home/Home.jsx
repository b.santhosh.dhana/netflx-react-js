import "./home.scss";
import Navbar from "../components/Navbar";

const home = () => {
  return (
    <div className="home">
      <Navbar />
      <img
        width="100%"
        src="https://images.pexels.com/photos/9940879/pexels-photo-9940879.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
        alt="profile pic"
      />
    </div>
  );
};

export default home;
